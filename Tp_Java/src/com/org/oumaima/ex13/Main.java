package com.org.oumaima.ex13;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	List <String> numbers=new ArrayList <> ();
		numbers.add("one");
		numbers.add("two");
		numbers.add("three");
		numbers.add("for");
		numbers.add("five");
		numbers.add("six");
		numbers.add("seven");
		numbers.add("eigth");
		numbers.add("nine");
		numbers.add("ten");
		numbers.add("eleven");
		numbers.add("twelve");
		int TailleChainePlusLongue=numbers.stream()
				.mapToInt(s->s.length())
				.max()
				.getAsInt();
		System.out.println(TailleChainePlusLongue);
		int NombreChaineTailleImpaire=(int) numbers.stream()
				.map(s->s.length())
				.filter(i->i%2==0)
				.count();
		System.out.println(NombreChaineTailleImpaire
				);
		List<String> listChaineImpaire = numbers.stream()
				.filter(s->(s.length()%2)==0)
				.map(String::toUpperCase)
				.collect(Collectors.toList());
		System.out.println(listChaineImpaire);
		String Chaine5caracterordonnée=numbers.stream()
												.filter(s->s.length()<=5)
												.sorted()
												.collect(Collectors.joining(",","{","}"));
		
		System.out.println(Chaine5caracterordonnée);
		Map <Integer,List<String>> MapLength=numbers.stream()
				.collect(Collectors.groupingBy(s->s.length()));
		System.out.println(MapLength);
		Map<String, String> MapFirstLetter=numbers.stream()
						.collect(Collectors.groupingBy(s->String.valueOf(s.charAt(0)),Collectors.joining(",")));
						
						System.out.println(MapLength);
		System.out.println(MapFirstLetter);
	}

}
