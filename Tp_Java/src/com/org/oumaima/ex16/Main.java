package com.org.oumaima.ex16;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		Person p1 = new Person("Kridane", "Oumaima", 23);
		Person p2 = new Person("Bouchniba", "Ryma", 2);
		Person p3 = new Person("Bouchniba", "Adem", 24);
		List<Person> lstPerson = new ArrayList<>();
		lstPerson.add(p1);
		lstPerson.add(p2);
		lstPerson.add(p3);
		
		PersonWriter pWriter = new PersonWriter();
		pWriter.writeBinaryFields(lstPerson, "filePerson");
		
		PersonReader pReader = new PersonReader();
		pReader.readBinaryFields("filePerson");

	}

}
