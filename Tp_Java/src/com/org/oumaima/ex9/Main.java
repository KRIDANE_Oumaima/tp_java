package com.org.oumaima.ex9;

import java.util.function.Predicate;



public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Predicate<String> StringLengthSup4= s->s.length()>4;
		Predicate <String> StringEmpty=s->s.isEmpty();
		Predicate <String> StringBeginJ=s->s.charAt(0)=='J';
		Predicate<String> StringLengthEqual5= s->s.length()==5;
		Predicate <String> StringLengthEqual5BeginJ=StringBeginJ.and(StringLengthEqual5) ;
		System.out.println(StringLengthSup4.test("oumaima"));
		System.out.println(StringEmpty.test("oumaima"));
		System.out.println(StringLengthEqual5.test("Java9"));
		System.out.println(StringLengthEqual5BeginJ.test("Java9"));


	}


}
