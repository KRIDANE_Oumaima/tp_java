package com.org.oumaima.ex7;
 public class Marin {
private String nom;
private String prenom;
private int salaire;

//question1
public Marin(String nom, String prenom, int salaire) {
	this.nom = nom;
	this.prenom = prenom;
	this.salaire = salaire;
}

//question2
public Marin(String nom, int salaire) {
		this(nom,"",salaire);
}

//question3
public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}

//question4
public String getPrenom() {
	return prenom;
}

public void setPrenom(String prenom) {
	this.prenom = prenom;
}

public int getSalaire() {
	return salaire;
}

public void setSalaire(int salaire) {
	this.salaire = salaire;
}

//question5
public int augmenteSalaire(int augmentation){
	return this.salaire=((this.salaire*augmentation)/100)+this.salaire;
}

//question7
@Override
public String toString() {
	return "Marin [nom=" + nom + ", prenom=" + prenom + ", salaire=" + salaire + "]";
}

//question8
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((nom == null) ? 0 : nom.hashCode());
	result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
	result = prime * result + salaire;
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Marin other = (Marin) obj;
	if (nom == null) {
		if (other.nom != null)
			return false;
	} else if (!nom.equals(other.nom))
		return false;
	if (prenom == null) {
		if (other.prenom != null)
			return false;
	} else if (!prenom.equals(other.prenom))
		return false;
	if (salaire != other.salaire)
		return false;
	return true;
}

public int compareTo(Marin o) {
	// TODO Auto-generated method stub
	if(o.nom.equals(o.nom)){
		return this.prenom.compareTo(o.prenom);
	}
	else 
		return this.nom.compareTo(o.nom);
}




}
