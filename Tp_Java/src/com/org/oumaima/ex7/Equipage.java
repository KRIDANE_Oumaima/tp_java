package com.org.oumaima.ex7;

import java.util.Collection;
import java.util.ArrayList;

public class Equipage {
 private Collection <Marin>  equipage;
private int nombreMax ;
 
 public Equipage(int nombreMax) {
	 this.nombreMax=nombreMax;
	equipage= new ArrayList<>();
}
public void 	addMarin(Marin m) {
if(getNombreMarins()<nombreMax) 
	equipage.add(m);

}
 public boolean 	removeMarin(Marin m) {

return equipage.remove(m);
 
}
 public boolean 	isMarinPresent(Marin m) {
 
 return equipage.contains(m);
 
}
@Override
public String toString() {
	return "Equipage [marins=" + equipage + "]";
} 
public void addAllEquipage(Equipage equip )
{
	for (Marin marin : equip.equipage) {
		if(!this.isMarinPresent(marin)) this.addMarin(marin);
	}
}
public void clear ()
{ 
	equipage.clear();
}
public int getNombreMarins()
{
return equipage.size();}
public float getMoyenneSalaire ()
{
	float m=0;
	for (Marin marin : equipage) {
		m=m+(((float)marin.getSalaire())/getNombreMarins());
	}
	
	return m;
}

}
