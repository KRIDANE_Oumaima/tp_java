package com.org.oumaima.ex7;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Equipage equipage=new Equipage(10);
		Marin m1=new Marin("Kridane","Oumaima",6000);
		 equipage.addMarin(m1);
		 Marin m2=new Marin("Rjiba","Haithem",4500);
		 equipage.addMarin(m2);
		 Marin m3=new Marin("Mned","Mehdi",3000);
		 equipage.addMarin(m3);
		 Marin m4=new Marin("Elamry","Mohammed",7000);
		 equipage.addMarin(m4);
		 Marin m5=new Marin("Salah","Ahmed",10000);
		 equipage.addMarin(m5);
		 System.out.println(equipage);
		 equipage.removeMarin(m1);
		 System.out.println(equipage);
		 System.out.println("le marin Kridane Oumaima est present ? "+equipage.isMarinPresent(m1));
		 equipage.addMarin(m1);
		 System.out.println("le marin Kridane Oumaima est present ? "+equipage.isMarinPresent(m1));
		 System.out.println(equipage);
	}

}
