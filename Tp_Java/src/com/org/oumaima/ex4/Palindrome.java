package com.org.oumaima.ex4;

public class Palindrome {
	
	public boolean palindrome (String phrase)
	{
		int l,i=0 ;
		char caract;
		//transformation en miniscule
		phrase=phrase.toLowerCase();
		l=phrase.length();
		//supression des caracteres speciaux
		while(i<l)
			{
			caract=phrase.charAt(i);
			if((caract!='a')&&(caract!='b')&&(caract!='c')&&
					(caract!='d')&&(caract!='e')&&(caract!='f')&&
					(caract!='g')&&(caract!='h')&&(caract!='i')&&
					(caract!='j')&&(caract!='h')&&(caract!='k')&&(
					caract!='l')&&(caract!='m')&&(caract!='n')&&
					(caract!='o')&&(caract!='p')&&(caract!='q')&&
					(caract!='r')&&(caract!='s')&&(caract!='t')&&
					(caract!='u')&&(caract!='v')&&(caract!='x')&&
					(caract!='y')&&(caract!='z')&&(caract!='w'))	
				{phrase=phrase.substring(0, i)+phrase.substring(i+1);
				l=l-1;}
			else {i++ ;}
				}
		l=phrase.length();
		//inversion de la phrase
		String reverse=new String ();
		 for ( int i1 = l- 1; i1 >= 0; i1-- )
	         reverse = reverse + phrase.charAt(i1);
		
	return phrase.equals(reverse) ;	
	}


}

	