package com.org.oumaima.ex14;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RomanDAO {
	
	public List<String> readLinesFrom(String fileName){
		Path path = Paths.get(fileName);
		try(Stream<String> lines = Files.lines(path)){
			
			return lines.collect(Collectors.toList());
		}catch (IOException e){
			System.out.println(e.getMessage());
		}
		return null;
		}
	//question 1
	public List <String> readlinesGerminal (List <String> lst,int Size) {
		return(lst.stream().skip(70).limit(Size-322).collect(Collectors.toList()));
		
	}
}
