package com.org.oumaima.ex14;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;



public class Main {

	public static void main(String[] args) {
		
		//
		RomanDAO dao = new RomanDAO();
		List<String> listGerminal=new ArrayList<>();
		System.out.println(dao.readLinesFrom("germinal").size());
		//Question 1
		listGerminal=dao.readlinesGerminal(dao.readLinesFrom("germinal"),dao.readLinesFrom("germinal").size());
		System.out.println("les lignes de germinal sont"+listGerminal);
		//Question 2
		long nbrLignesNonVide ;
		nbrLignesNonVide=listGerminal.stream().filter(s-> !s.isEmpty()).count();
		System.out.println("le nombre des lignes non vide ::"+nbrLignesNonVide);
		//Question 3
		BiFunction<String,String,Integer> Contient= (line,mot)->line.split(mot).length -1;
		
		System.out.println("Le nombre des bonjour est "+listGerminal.stream().mapToInt(s->Contient.apply(s.toLowerCase(),"bonjour")).sum());
		//Question 4
		Function <String,Stream<Character>> streamChar =s->s.chars().mapToObj(c->(char)c);
		//Question 5
		 
		Stream<Character> LingnestoChar = streamChar.apply(listGerminal.toString());
		//Question 6
		System.out.println("Les caracters  dans ce fichier sont"+LingnestoChar.sorted().distinct().collect(Collectors.toList()));
		//question7
				BiFunction<String, String, Stream<String>> splitWordWithPattern = (line, pattern)-> Pattern.compile("["+pattern+"]").splitAsStream(line);
				
				String strGerminal= dao.readLinesFrom("germinal").stream()
																.filter(s->!s.isEmpty())
																.collect(Collectors.toList())
																.toString();
				
				String strFiltre = " !#$%()*+,-./0123456789:;<>?@\\[\\]_~";
				
				String strGerminalDistinct = dao.readLinesFrom("germinal").stream()
																		.filter(s->!s.isEmpty())
																		.distinct()
																		.collect(Collectors.toList())
																		.toString();

				System.out.println("Nombre de mots: "+splitWordWithPattern.apply(strGerminal, strFiltre).count());
				System.out.println("Nombre de mots diff�rents : "+splitWordWithPattern.apply(strGerminalDistinct, strFiltre).count());
				
				//question8
				String strLong = splitWordWithPattern.apply(strGerminal, strFiltre)
													.sorted(Comparator.comparing(String::length))
													.reduce((first, second) -> second)
													.get()
													.toString();
				System.out.println("le mot le plus long est : "+strLong);
				System.out.println("la longueur maximale est : : "+strLong.length());
				
				int numbLongStr = (int) splitWordWithPattern.apply(strGerminal, strFiltre).filter(s->s.length()==strLong.length()).distinct().count();
				System.out.println("nombre de mots les plus longs: "+numbLongStr);
				
				splitWordWithPattern.apply(strGerminal, strFiltre).filter(s->s.length()==strLong.length()).distinct().forEach(s->System.out.println(s));
				
				//question9
				Map<Integer, Long> histogramOnWordsLength =  new HashMap<>();
				histogramOnWordsLength = splitWordWithPattern.apply(strGerminal, strFiltre)
						  									.distinct()
														  	.filter(s -> (!s.isEmpty()) && (s.length() >= 2))
															.collect(Collectors.groupingBy(word -> word.length(), Collectors.counting()));
				
				System.out.println("l'histogramme de germinal");
				histogramOnWordsLength.forEach((key, value) -> System.out.println(key + "\t|\t" + value + "\t|"));
				
				//question10
				System.out.println("La longueur  m�diane est 10 : et le nombre des mots ayant cette longeur est  "+ histogramOnWordsLength.get(10) +" mots de cette longueur.");	
	}

}
