package com.org.oumaima.ex11;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;




public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Comparator <String> CompareLength =(s1,s2)->s1.length()-s2.length();
	Comparator <Person> CompareLastName =(p1,p2)->(p1.getLastName()).compareTo(p2.getLastName());
	Comparator <Person> CompareFirstName =(p1,p2)->(p1.getFirstName()).compareTo(p2.getFirstName());

	Comparator <Person> CompareLastFirstName=CompareLastName.thenComparing(CompareFirstName) ;
	Comparator <Person> CompareLastFirstNameRev=CompareLastFirstName.reversed() ;

	
	System.out.println(CompareLength.compare("Oumaima", "Ryma")  );
	Person p1=new Person ();
	Person p2=new Person ();
	p1.setLastName("Kridane");
	p2.setLastName("Kridane");
	System.out.println(CompareLastName.compare(p1, p2));
	Person p3=new Person ();
	Person p4=new Person ();
	p3.setFirstName("Oumaima");
	p4.setFirstName("Ryma");
	p3.setLastName("Kridane");
	p4.setLastName("Kridane");
	System.out.println(CompareLastFirstName.compare(p3,p4));
	System.out.println(CompareLastFirstNameRev.compare(p3,p4));
	List<Person> persons=new ArrayList<Person>();
	Person p6=new Person ();
	Person p5=new Person ();
	p6.setFirstName("Adem");
	p5.setFirstName("Mayma");
	p6.setLastName("Bouchniba");
	p5.setLastName("Krichniba");
	persons.add(p3);
	persons.add(p4);
	persons.add(p5);
	persons.add(p6);
	persons.sort(Comparator.nullsLast(CompareLastFirstName) );
	
	}

}
