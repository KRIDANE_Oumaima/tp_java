package com.org.oumaima.ex10;

import java.util.function.BiFunction;
import java.util.function.Function;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Function <String,String> UpCase=s -> s.toUpperCase();
		Function <String,String> StringId =s->s;
		Function <String,Integer> StringLength =s->s.length();
		Function <String,String> StringParenthese =s->'('+s+')';
		BiFunction <String,String,Integer> S1ContainS2=(String s1,String s2)->s1.indexOf(s2);
		Function <String,Integer> abcdefghiContainS=s->S1ContainS2.apply("abcdefghi",s);
		System.out.println(UpCase.apply("Oumaima"));
		System.out.println(StringId.apply("Oumaima"));
		System.out.println(StringLength.apply("Oumaima"));
		System.out.println(StringParenthese.apply("Oumaima"));
		System.out.println(S1ContainS2.apply("Oumaima","ma"));
		System.out.println(abcdefghiContainS.apply("bc"));
		

	}

}
