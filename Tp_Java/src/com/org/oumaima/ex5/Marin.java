package com.org.oumaima.ex5;
 
public class Marin{
	String nom ;
	String prenom ;
	int salaire ;
//le premier constructeur
public  Marin(String nom, String prenom,int salaire)
		{	
		this.nom=nom;
		this.prenom=prenom;
		this.salaire=salaire;
		}

//le deuxieme constructeur 
public Marin(String nom,int salaire)
{
	Marin marin=new Marin(nom,"",salaire);
	this.nom=marin.prenom;
	this.prenom=marin.prenom;
	this.salaire=marin.salaire;
}
//setter et getter pour nom
public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}
//setter et getter pour prenom et salaire 
public String getPrenom() {
	return prenom;
}

public void setPrenom(String prenom) {
	this.prenom = prenom;
}

public int getSalaire() {
	return salaire;
}

public void setSalaire(int salaire) {
	this.salaire = salaire;
}
//augmentation de salaire
public int augmenteSalaire (int augmentation)
{
	this.salaire=this.salaire+augmentation;
	return this.salaire ;
	}

//m�thode equals pour la comparaison 

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((nom == null) ? 0 : nom.hashCode());
	result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
	result = prime * result + salaire;
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (!(obj instanceof Marin))
		return false;
	Marin other = (Marin) obj;
	if (nom == null) {
		if (other.nom != null)
			return false;
	} else if (!nom.equals(other.nom))
		return false;
	if (prenom == null) {
		if (other.prenom != null)
			return false;
	} else if (!prenom.equals(other.prenom))
		return false;
	if (salaire != other.salaire)
		return false;
	return true;
}
public int compareTo (Marin m) {
	int comp ;//comp contient le r�sultatat de comp	araison:n�gative dans le cas ou la marin 1 est inf au deuxime � en cas d'�galit� positve sinon.
	comp=this.nom.compareTo(m.nom);
	if (comp==0) comp=this.prenom.compareTo(m.prenom);
	return comp ;
}
}
