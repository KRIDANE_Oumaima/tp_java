package com.org.oumaima.ex15;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;

public class PersonWriter {


	Function<Person, String> personToString = p -> "\n"+p.getLastName()+","+p.getFirstName()+","+p.getAge();
	boolean write(List<Person> people, String fileName){
		File file = new File(fileName);
		try (FileWriter fileWriter = new FileWriter(file,true);
				BufferedWriter writer = new BufferedWriter(fileWriter);){
			for (Person person : people) {
				writer.write(personToString.apply(person));
			}
			return true;
		}catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}

	}



}
