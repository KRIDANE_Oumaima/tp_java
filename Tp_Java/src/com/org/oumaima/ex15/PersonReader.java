package com.org.oumaima.ex15;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersonReader {
	Predicate<String> comment = s -> s.startsWith("#");
	Function <String, Person> readLine= s -> new Person(s.split(",")[0],s.split(",") [1],Integer.parseInt(s.split(",")[2]));
	Function<String, Stream<Person>> readLineToStream = string -> comment.test(string) ? Stream.empty() : Stream.of(new Person(string.split(",")[0],
			string.split(",")[1],
			Integer.parseInt(string.split(",")[2])));

	List<Person> read (String fileName) throws IOException{

		List<Person> persons = new ArrayList<>();
		File file = new File(fileName);
		try (FileReader fileReader = new FileReader(file);
				BufferedReader reader = new BufferedReader(fileReader);){

			persons = reader.lines()
					.filter(s->!s.isEmpty())
					.flatMap(readLineToStream)
					.collect(Collectors.toList());


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return persons;
	}

	public List<Person>readfromStream(String fileName){
		List <Person> listpersons= new ArrayList<>();
		BufferedReader brPerson;
		try {
			brPerson = new  BufferedReader(new FileReader(fileName));
			listpersons=brPerson
					.lines()
					.filter(s-> !s.startsWith("#"))
					.flatMap(readLineToStream)
					.collect(Collectors.toList());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		return listpersons;

	}

}

