package com.org.oumaima.ex15;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Main {

	public static void main(String[] args)  {

		PersonReader reader = new PersonReader();
		try {

			System.out.println(reader.read("PersonsFile.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		Person p1 = new Person("Mned", "Mehdi", 23);
		Person p2 = new Person("Elamry","Mohamed" , 24);
		Person p3 = new Person("Salah", "Ahmed", 24);
		Person p4 = new Person("Rjiba", "Haithem", 23);

		List<Person> listPerson = new ArrayList<>();
		listPerson.add(p1);
		listPerson.add(p2);
		listPerson.add(p3);
		listPerson.add(p4);

		PersonWriter writer = new PersonWriter();
		writer.write(listPerson, "PersonsFile.txt");
	}

}
