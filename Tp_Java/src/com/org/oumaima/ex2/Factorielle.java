package com.org.oumaima.ex2;

import java.math.BigInteger;

public class Factorielle {
public int intFactorielle (int i)
{
	int fact=1 ;
	for (int j = 2; j < i; j++)
	{
	fact=fact*j;
	}
	return fact ;
	
}
public double doubleFactorielle (int i)
{
	double fact=1 ;
	for (int j = 2; j < i; j++)
	{
	fact=fact*j;
	}
	return fact ;
	
}
public BigInteger bigIntFactorielle(int n) {
    BigInteger produit = BigInteger.ONE;
    BigInteger mul = BigInteger.ONE;
    for (int i = 1; i <= n; i++ , mul = mul.add(BigInteger.ONE))
        produit = produit.multiply(mul);
    return produit;
}

}
