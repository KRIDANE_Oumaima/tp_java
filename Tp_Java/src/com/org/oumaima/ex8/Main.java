package com.org.oumaima.ex8;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RegistreMarin registrem=new RegistreMarin();
		Marin m0=new Marin("Lamazou","Titouan", 0);
		m0.setAnnee(1995);
		Marin m4=new Marin("Gautier","Alain", 0);
		m4.setAnnee(1962);
		Marin m5=new Marin("Augin","Christophe", 0);
		m5.setAnnee(1959);
		Marin m6=new Marin("Le Cl�ac'h","Armel", 0);
		m6.setAnnee(1977);
		Marin m1=new Marin("Desjoyeux","Michel", 0);
		m1.setAnnee(1965);
		Marin m2=new Marin("Riou","Vincent", 0);
		m2.setAnnee(1972);
		Marin m3=new Marin("Fran�ois","Gabart", 0);
		m3.setAnnee(1983);
		registrem.addMarin(m1);
		registrem.addMarin(m2);
		registrem.addMarin(m3);
		registrem.addMarin(m4);
		registrem.addMarin(m5);
		registrem.addMarin(m6);
		registrem.addMarin(m0);
		System.out.println(registrem);
		System.out.println("Le nombre des marins n�s pendant la d�cinie 1970 est "+registrem.count(1970)+" ce sont :"+registrem.get(1970));
		System.out.println("Le nombre des marins n�s pendant la d�cinie 1950 est "+registrem.count(1950));
		System.out.println("Le nombre des marins n�s pendant la d�cinie 1960 est "+registrem.count(1960));
		System.out.println("Le nombre des marins n�s pendant la d�cinie 1980 est "+registrem.count(1980));
		System.out.println("Le nombre total des marins  "+registrem.count());
	}

}
