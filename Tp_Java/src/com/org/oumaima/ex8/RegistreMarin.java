package com.org.oumaima.ex8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;




public class RegistreMarin {
private Map<Integer, ArrayList<Marin>> registre =new HashMap<Integer,ArrayList<Marin>>();
public void addMarin(Marin marin)
{
	Integer dec=(marin.getAnnee()/10)*10;
	if(registre.containsKey(dec)) 
		(registre.get(dec)).add(marin);
	else { ArrayList<Marin> l=new ArrayList<Marin> ();
			l.add(marin);
			registre.put(dec, l);
			
	}

}
public ArrayList<Marin> get (int dec)
{
	 Integer Dec=new Integer(dec) ;
	return registre.get(Dec);

}
public int count (int dec)
{
	 Integer Dec=new Integer(dec) ;
	return registre.get(Dec).size();

}
public int count ()
{
	int somme=0;
	for (ArrayList<Marin > l : registre.values()) {
		somme=somme+l.size();
	}
	return somme;

}
@Override
public String toString() {
	return "RegistreMarin [registre=" + registre + "]";
}

}

